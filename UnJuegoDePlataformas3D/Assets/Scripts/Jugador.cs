﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Jugador : MonoBehaviour
{
    public AudioSource sonidoDisparo;
    public Text vida;
    public Text municion;
    public float vidaMaxima = 100;
    public float vidaActual;
    public int balas = 50;
    public float alcance = 10f;
    private Animator Animador;
    private float fuerzaDisparo = 25f;
    private float tiempoEntreDisparos = 1.0f;
    private float tiempoTranscurridoDisparo = 0;
    private float tiempoTranscurridoMuerte = 0;
    bool disparo;
    bool impacto;
    bool morir;
    void Start()
    {
        Animador = GetComponent<Animator>();
        vidaActual = vidaMaxima;
        tiempoEntreDisparos = 1.0f;
        
    }

    void FixedUpdate()
    {
        // Se pinta una línea en el Debug para comprobar si está disparando hacia el lugar adecuado
        Debug.DrawRay (new Vector3 (transform.position.x, transform.position.y + 1.5f, transform.position.z), transform.forward * alcance, Color.red);
        // Se empieza a sumar el tiempo transcurrido
        tiempoTranscurridoDisparo += Time.deltaTime;
        // Si se pulsa el botón del ratón y ha transucrrido el tiempo mínimo, y además al jugador le quedan balas, se produce un disparo
        if (Input.GetMouseButton(0)){
            if(tiempoTranscurridoDisparo > tiempoEntreDisparos && balas > 0)
            {
                tiempoTranscurridoDisparo = 0;
                disparo = true;
                Disparar();
            }
        }
        if (morir){
            tiempoTranscurridoMuerte += Time.deltaTime;
            if(tiempoTranscurridoMuerte > 5f){
                SceneManager.LoadScene("GameOver");
            }
        }
    }
    
    // La función de disparar
    public void Disparar(){
        // Resta una bala del total de munición
        balas -= 1;
        // Ejecuta la animación de disparar
        ActualizarAnimacion();
        // Crea un raycast de una cierta longitud y mira con qué colisiona
        RaycastHit hit;
        if (Physics.Raycast(new Ray (new Vector3 (transform.position.x, transform.position.y + 1.5f, transform.position.z), transform.forward * alcance), out hit))
        {
            // Si la colisión es con un enemigo se ejecutará su función de recibir impacto
            if(hit.collider.gameObject.tag == "Enemigo"){
                hit.collider.GetComponent<Zombie>().Impacto(fuerzaDisparo, hit.collider.transform.position);
            }
        }
        // Se actualiza la cantidad de balas en el HUD
        municion.text = balas.ToString("00");
        // Se reproduce el sonido de disparo con un cierto retraso para que coincida con la animación
        sonidoDisparo.PlayDelayed(0.3f);
    }
    // La función morir
    public void Morir(){
        // Se reproduce la animación de morir y lulego se carga la escena del GameOver
        morir = true;
        ActualizarAnimacion();
        //SceneManager.LoadScene("GameOver");

    }
    // Si el jugador recibe un impacto se le resta vida y se actualiza en el HUD
    public void Impacto(float fuerzaGolpe)
    {
        impacto = true;
        vidaActual -= fuerzaGolpe;
        vida.text = vidaActual.ToString("00");
        if (vidaActual <= 0)
        {
            Morir();
        }  
        ActualizarAnimacion();
    }
    // Función para devolver la cantidad de vida que le queda al jugador para que no siga atacándole el zombi que lo ha matado una vez se queda sin vida
    public float CantidadVida(){
        return vidaActual;
    }

    // Cuando sucede alguna de las circunstancias mencionadas se ejecuta su animación
    void ActualizarAnimacion(){
        if (disparo){
            Animador.SetTrigger("Disparar");
            disparo = false;
        }
        if(impacto){
            Animador.SetTrigger("Impacto");
            impacto = false;
        }
        if (morir){
            Animador.SetTrigger("Morir");
            GetComponent<CapsuleCollider>().height = 0.3f;
            GetComponent<CapsuleCollider>().material = null;
        }
    }

    // Cuando el jugador recoge un paquete de munición se le suman 10 balas al total y se actualiza en el HUD
    public void CogerMunicion(){
        balas = balas + 10;
        municion.text = balas.ToString("00");
    }

    // Cuando el jugador recoge un paquete de vida se le suma 10 a su vida o hasta el máximo si lo supera y se actualiza en el HUD
    public void CogerVida(){
        vidaActual = vidaActual + 10;
        if (vidaActual > vidaMaxima){
            vidaActual = vidaMaxima;
        }
        vida.text = vidaActual.ToString("00");
        
    }
}
