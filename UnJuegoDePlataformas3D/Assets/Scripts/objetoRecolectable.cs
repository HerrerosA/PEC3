﻿using System.Collections;
using UnityEngine;

public class objetoRecolectable : MonoBehaviour
{
    public string tipo;
    private bool recogido = false;
    void Update(){
        if (recogido == true){
            Destroy(this.transform.parent.gameObject);
        }
    }
    void OnTriggerEnter(Collider col){
        if (col.gameObject.tag == "Player"){ 
            recogido = true;
            if (tipo == "balas"){
                col.gameObject.GetComponent<Jugador>().CogerMunicion();
            }
            if (tipo == "vida"){
                col.gameObject.GetComponent<Jugador>().CogerVida();
            }
            
        }
        
    }
}
