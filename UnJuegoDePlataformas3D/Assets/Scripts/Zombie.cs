﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    public float radioDeambular;
    public float tiempoDeambular;
    public float vida = 100;
    public GameObject disparo;
    public GameObject mano;
    GameObject[] ultimoDisparo;
    private Animator Animador;

    private Transform objetivo;
    private NavMeshAgent agente;
    private float temporizador;
    private float tiempoDesdeMuerte = 0;
    private float tiempoDesdeDisparo = 0;
    private float fuerzaGolpe = 10f;
    bool ataque;
    private float tiempoDesdeAtaque = 0;
    bool impacto;
    bool morir;
    bool caminar = true;
    bool perseguir = false;
    int actual = 0;
    private GameObject jugador;

    // Al empezar se coge el NavMeshAgent y el animador y se le pone a caminar, también se instancias los sistemas de partículas de los disparos
    void Start(){
        agente = this.GetComponent<NavMeshAgent>();
        Animador = this.GetComponent<Animator>();
        caminar = true;
        temporizador = tiempoDeambular;
        ultimoDisparo = new GameObject[2];
        jugador = null;
    }

    void Update(){
        // se mira si el zombie debe estar caminando, si es así, cada cierto tiempo se busca una posición cercana dentro de una esfera alrededor y se fija como destino esa posición. Cuando lleva cierto tiempo yendo hacia esa dirección vuelve a buscar otra y la fija como destino
        if (caminar && !morir){
            temporizador += Time.deltaTime;
            if(temporizador >= tiempoDeambular){
                Vector3 nuevaPosicion = RandomNavSphere(transform.position, radioDeambular, -1);
                agente.SetDestination(nuevaPosicion);
                temporizador = 0;
            }
        }
        if (perseguir && !morir){
            Vector3 nuevaPosicion = jugador.transform.position;
            agente.SetDestination(nuevaPosicion);
        }
        if (ataque == true && !morir){
            tiempoDesdeAtaque += Time.deltaTime;
            if (tiempoDesdeAtaque > 1.5f)
            {
                ataque = false;
                perseguir = true;
                caminar = false;
                Vector3 nuevaPosicion = jugador.transform.position;
                //agente.enabled = true;
                agente.SetDestination(nuevaPosicion);
            }
        }
        if(jugador != null){
            if (Vector3.Distance(jugador.transform.position,transform.position)<2.5f && !ataque){                
                ataque = true;
                caminar = false;
                perseguir = false;
                tiempoDesdeAtaque=0;
                Atacar();
            }
        }
        
        // si recibe un impacto se empieza a contar el tiempo para luego poder destruir el sistema de partículas generado
        if (impacto == true){
            tiempoDesdeDisparo += Time.deltaTime;
            if (tiempoDesdeDisparo > 0.8f){
                Destroy(ultimoDisparo[actual]);
                impacto = false;
            }

        }
        // si el zombie es abatido se empieza a contar el tiempo para volver a aparecer en otro lugar
        if (morir == true) {
            caminar = false;
            tiempoDesdeMuerte += Time.deltaTime;
            if (tiempoDesdeMuerte > 4f){
                Reaparecer();
            }
        }
    }

    void Atacar (){
        ActualizarAnimacion();
        RaycastHit hit;
        if (Physics.SphereCast(mano.transform.position, 0.3f, mano.transform.forward, out hit)){
            if(hit.collider.gameObject.tag == "Player"){
                if(hit.collider.GetComponent<Jugador>().CantidadVida() <= 10f){
                    enabled = false;
                    GetComponent<Animator>().enabled = false;
                }
                hit.collider.GetComponent<Jugador>().Impacto(fuerzaGolpe);
                
            }
        }
    }

    void OnTriggerEnter(Collider col){
        if(col.gameObject.tag == "Player"){
            jugador = col.gameObject;
            perseguir = true;
            caminar = false;
        }
    }
    void OnTriggerExit(Collider col){
        if(col.gameObject.tag == "Player"){
            perseguir = false;
            ataque = false;
            caminar = true;
        }
    }

    // cuando se llama la función reaparecer se vuelve a poner la vida del zombie a tope, se busca un punto aleatorio dentro del mapa para reaparecer y se le pone a caminar. Se vuelve a activar el navmeshAgent, desactivado para que la animación de morir sea más creíble
    private void Reaparecer (){
        vida = 100;
        var posx = Random.Range(-100, 100);
        var posz = Random.Range(-100, 100);
        transform.position = new Vector3(posx, 0, posz);
        agente.enabled = true;
        caminar = true;
        morir = false;
        Start();
    }
    
    // Función para elegir un punto cercano para fijar el nuevo destino del zombie al caminar
    private static Vector3 RandomNavSphere(Vector3 origen, float distancia, int capa){
        Vector3 direccionAleatoria = Random.insideUnitSphere * distancia;
        direccionAleatoria += origen;
        NavMeshHit navHit;
        NavMesh.SamplePosition(direccionAleatoria, out navHit, distancia, capa);
        return navHit.position;
    }

    // Cuando el zombie recibe un impacto se le resta vida, se instancia un nuevo sistema de partículas que hará parecer que recibe un disparo
    public void Impacto(float fuerzaDisparo, Vector3 posicion){
        vida -= fuerzaDisparo;
        ultimoDisparo[actual] = Instantiate(disparo, new Vector3(posicion.x, posicion.y+1.5f, posicion.z), Quaternion.identity);
        actual ++;
        if (actual == 2) actual = 0;
        // Si al restarse la vida llega a 0 entonces el zombie muere
        if (vida <= 0){
            morir = true;
            agente.enabled = false;
            ActualizarAnimacion();
        }
        // Se ejecuta la animación de recibir impacto
        impacto = true;
        ActualizarAnimacion();
    }
    
    // Cuando sucede alguna de las circunstancias mencionadas se ejecuta su animación
    void ActualizarAnimacion(){
        if (ataque){
            Animador.SetTrigger("Atacar");
        }
        if(impacto){
            Animador.SetTrigger("Impacto");
        }
        if (morir){
            caminar = false;
            Animador.SetTrigger("Morir");
            
        }
    }
}
